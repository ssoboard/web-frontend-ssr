# Project - SSOBoard

## Router
- GET - / (Home)
- GET - /auth/signin (Sign In)
- GET - /auth/signup (Sign Up)
- GET - /dashboards (Dashboard List)
- GET - /dashboard/:type/:id (Dashboard Details)

## Fetch (http://localhost:3300)
- POST - /auth/signin
- POST - /auth/signup
- GET - /auth/signout
- GET - /auth/me
- GET - /notifications
- GET - /notification/:ids
- GET - /dashboard/github/:id/repositories
- GET - /dashboard/github/:id/repository/:repo_id
- GET - /dashboard/gitlab/:id/repositories
- GET - /dashboard/gitlab/:id/repository/:repo_id

## Build
```
$ yarn build
```