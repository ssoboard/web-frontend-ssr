import React from 'react';

import Header from '../src/layout/Header/component/Header.jsx';
import SideBar from '../src/layout/SideBar/container/Sidebar';

export default function HomePage() {
  return (
    <>
      <Header />
      <SideBar />
      <section id="board">
        <div className="board">
          <div className="contents">
          </div>
        </div>
      </section>
    </>
  )
}