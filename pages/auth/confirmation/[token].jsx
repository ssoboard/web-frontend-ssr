import Axios from 'axios';
import { Router, useRouter } from 'next/router';
import React from 'react';

export default function VerifyEmail() {
  const router = useRouter();
  const token = router.query.token;

  Axios
  .get(`http://localhost:3300/auth/confirmation/${token}`)
  .then(data => {});

  return (
    <>
      <h2>이메일 인증이 완료되었습니다.</h2>
      <p>{token}</p>
    </>
  );  
}