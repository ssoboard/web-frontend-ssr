import { Router, useRouter } from 'next/router';
import React from 'react';
import SignIn from '../../src/layout/Auth/container/SignIn';
import SignUp from '../../src/layout/Auth/container/SignUp';

export default function AuthPage() {
  const router = useRouter();
  const type = router.query.type;

  return (
    <>
      { type === 'signin' ? <SignIn />
      : type === 'signup' ? <SignUp />
      : ''}
    </>
  );
}