import React from 'react';

import Header from '../../src/layout/Header/component/Header';
import SideBar from '../../src/layout/SideBar/container/Sidebar';

import GithubDashboard from '../../src/layout/Dashboard/container/GithubDashboard';
import GitlabDashboard from '../../src/layout/Dashboard/container/GitlabDashboard';
import CodingTestDashboard from '../../src/layout/Dashboard/container/CodingTestDashboard';
import UXUIDashboard from '../../src/layout/Dashboard/container/UXUIDashboard';
import WebpageDashboard from '../../src/layout/Dashboard/container/WebpageDashboard';
import CourseraDashboard from '../../src/layout/Dashboard/container/CourseraDashboard';

import { useRouter } from 'next/router';

export default function DashboardPage() {
  const router = useRouter();
  let type = '';
  let id = '';
  
  if(router.query.param){
    type = router.query.param[0];
    id = router.query.param[1];
  }

  return (
    <>
      <Header />
      <SideBar selectedId={id} />
      <section id="board">
        <div className="board">
          <div className="contents">
            { type === 'gitlab' ? <GitlabDashboard />
            : type === 'github' ? <GithubDashboard />
            : type === 'codingtest' ? <CodingTestDashboard />
            : type === 'uxui' ? <UXUIDashboard />
            : type === 'webpage' ? <WebpageDashboard />
            : type === 'coursera' ? <CourseraDashboard />
            : ''}
          </div>
        </div>
      </section>
    </>
  )
}