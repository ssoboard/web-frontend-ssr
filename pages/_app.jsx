import '../public/styles/index.scss';
import React from 'react';
import App, {AppInitialProps, AppContext} from 'next/app';
import {END} from 'redux-saga';
import { wrapper } from '../src/layout/Common/store';

class WrappedApp extends App {
  render() {
    const {Component, pageProps} = this.props;
    return <Component {...pageProps} />;
  }
}

export default wrapper.withRedux(WrappedApp);