import Axios from 'axios';

const SSOBoardAPI = (function(){
  const axios = Axios.create({
    baseURL: 'http://localhost:3300/',
    withCredentials: true
  });

  return {
    // Auth
    signUp: (params) => axios.post(`/auth/signup`, params),
    confirmation: (params) => axios.get(`/auth/confirmation/${params.email_verify_token}`),
    signIn: (params) => axios.post(`/auth/signin`, params),
    signOut: () => axios.get(`/auth/signout`),
    resetPassword: (params) => axios.post(`/auth/reset_password`, params),
    changePassword: (params) => axios.post(`/auth/change_password/${params.password_reset_token}`, params),

    // GitHub
    // GitLab
    // CodingTest
    // Coursera
    // Notification
  };
})();

export default SSOBoardAPI;