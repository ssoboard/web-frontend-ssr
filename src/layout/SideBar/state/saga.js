import { all, call, put, takeLeading } from 'redux-saga/effects';
import { Types, actions } from '.';

function* fetchMyDashboards() {
  yield put(actions.setValue('myDashboards', [
    {id: 0, type:'GIT_HUB', link:'/dashboard/github', title:'Github'},
    {id: 1, type:'GIT_LAB', link:'/dashboard/gitlab', title:'Gitlab'},
    {id: 2, type:'COURSERA', link:'/dashboard/coursera', title:'Coursera'},
    {id: 3, type:'CODING_TEST', link:'/dashboard/codingtest', title:'Coding Test'},
  ]));
}

export default function* () {
  yield all([
    takeLeading(
      Types.FETCH_MY_DASHBOARDS,
      fetchMyDashboards
    ),
  ]);
}
