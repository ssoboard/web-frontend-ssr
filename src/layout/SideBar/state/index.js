import {
  createReducer,
  createSetValueAction,
  setValueReducer,
  NOT_IMMUTABLE,
} from '../../Common/redux-helper';

export const Types = {
  SET_VALUE: 'sidebar/SET_VALUE',
  FETCH_MY_DASHBOARDS: 'sidebar/FETCH_NOTIFICATIONS',
  INITIALIZE: 'sidebar/INITIALIZE',
};

export const actions = {
  // ReduxActions
  setValue: createSetValueAction(Types.SET_VALUE),
  initialize: () => ({ type: Types.INITIALIZE, [NOT_IMMUTABLE]: true }),

  // SagaActions
  fetchMyDashboards: () => ({
    type: Types.FETCH_MY_DASHBOARDS
  }),
};

const initialState = {
  myDashboards: []
};

const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});

export default reducer;
