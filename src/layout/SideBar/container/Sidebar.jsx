// Library
import React, { useEffect } from 'react';
import { actions } from '../state';
import { useDispatch, useSelector } from 'react-redux';

// Icons
import { AiFillCode } from "react-icons/ai";
import { BiMessageSquareAdd } from "react-icons/bi";
import { FaGithub, FaGitlab } from "react-icons/fa";
import { SiCoursera, SiVisualstudiocode } from "react-icons/si";

// Components
import Line from '../component/Line';
import Link from 'next/link';
import Router from 'next/router';

export default function SideBar({ selectedId='' }) {
  // Dispatch
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchMyDashboards());
  }, [dispatch])
  
  // State
  const myDashboards = useSelector(state => state.sidebar.myDashboards);

  // Event
  function onShowModal(){
    dispatch(actions.setValue('modalVisible', true));
  }
  function onClickMenu (e, data) {
    Router.push(`${data.link}/${data.id}`);
  }

  // Rendering
  return (
    <>
      <aside id="side-bar" className="side-bar">
        <h2 className="title clear">
          <div className="text">My Dashboard</div>
          <div className="icons" onClick={onShowModal}>
            <BiMessageSquareAdd/>
          </div>
        </h2>
        <ul className="list">
          {myDashboards.map(data => (
            // <Link href={data.link}>
            <li key={data.id}
              className={
                selectedId !== '' && Number(selectedId) === data.id
                ? "menu clear on"
                : "menu clear"
              }
              onClick={(e) => {onClickMenu(e, data)}}>
              {data.type === 'GIT_HUB' ? <FaGithub />
              :data.type === 'GIT_LAB' ? <FaGitlab />
              :data.type === 'COURSERA' ? <SiCoursera />
              :data.type === 'CODING_TEST' ? <AiFillCode />
              :''}
              <span className="text">{data.title}</span>
            </li>
            // </Link>
          ))}
        </ul>
      </aside>
      <Line />
    </>
  )
}