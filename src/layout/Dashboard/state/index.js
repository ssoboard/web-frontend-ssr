import {
  createReducer,
  createSetValueAction,
  setValueReducer,
  NOT_IMMUTABLE,
} from '../../Common/redux-helper';

export const Types = {
  SET_VALUE: 'dashboard/SET_VALUE',

  FETCH_GITHUB_REPOSITORIES: 'dashboard/FETCH_GITHUB_REPOSITORIES',
  FETCH_GITHUB_BRANCHES: 'dashboard/FETCH_GITHUB_BRANCHES',
  FETCH_GITHUB_ISSUES: 'dashboard/FETCH_GITHUB_ISSUES',
  FETCH_GITHUB_PULL_REQUEST: 'dashboard/FETCH_GITHUB_PULL_REQUEST',

  FETCH_GITLAB_REPOSITORIES: 'dashboard/FETCH_GITLAB_REPOSITORIES',
  FETCH_GITLAB_BRANCHES: 'dashboard/FETCH_GITLAB_BRANCHES',
  FETCH_GITLAB_ISSUES: 'dashboard/FETCH_GITLAB_ISSUES',
  FETCH_GITLAB_MERGE_REQUEST: 'dashboard/FETCH_GITLAB_MERGE_REQUEST',

  INITIALIZE: 'dashboard/INITIALIZE',
};

export const actions = {
  // ReduxActions
  setValue: createSetValueAction(Types.SET_VALUE),
  initialize: () => ({ type: Types.INITIALIZE, [NOT_IMMUTABLE]: true }),

  // SagaActions - Github
  fetchGithubRepositories: (user_id) => ({ type: Types.FETCH_GITHUB_REPOSITORIES, user_id }),
  fetchGithubBranches: (repo_id) => ({ type: Types.FETCH_GITHUB_BRANCHES, repo_id }),
  fetchGithubIssues: (repo_id) => ({ type: Types.FETCH_GITHUB_ISSUES, repo_id }),
  fetchGithubPullRequest: (repo_id) => ({ type: Types.FETCH_GITHUB_PULL_REQUEST, repo_id }),
  
  // SagaActions - Gitlab
  fetchGitlabRepositories: (user_id) => ({ type: Types.FETCH_GITLAB_REPOSITORIES, user_id }),
  fetchGitlabBranches: (repo_id) => ({ type: Types.FETCH_GITLAB_BRANCHES, repo_id }),
  fetchGitlabIssues: (repo_id) => ({ type: Types.FETCH_GITLAB_ISSUES, repo_id }),
  fetchGitlabMergeRequest: (repo_id) => ({ type: Types.FETCH_GITLAB_MERGE_REQUEST, repo_id }),
};

const initialState = {
  // Github
  githubRepositories:[],
  githubBranches:[],
  githubIssues:[],
  githubPullRequests:[],

  // Gitlab
  gitlabRepositories:[],
  gitlabBranches:[],
  gitlabIssues:[],
  gitlabMergeRequests:[],
};

const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});

export default reducer;
