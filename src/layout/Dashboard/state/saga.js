import { all, call, put, takeLeading } from 'redux-saga/effects';
import { Types, actions } from '.';

function* fetchGithubRepositories({ user_id }) {
  yield put(actions.setValue('githubRepositories', [
    {
      "id": 1,
      "node_id": "MDEwOlJlcG9zaXRvcnkxMjk2MjY1",
      "name": "React",
      "full_name": "horong/template-react",
      "private": false,
      "html_url": "https://github.com/horong/template-react",
      "description": "This your first repo!",
    },
    {
      "id": 2,
      "node_id": "MDEwOlJlcG9zaXRvcnkxMjk2MjY2",
      "name": "React + Typescript",
      "full_name": "horong/template-react-typescript",
      "private": false,
      "html_url": "https://github.com/horong/template-react-typescript",
      "description": "This your first repo!",
    },
    {
      "id": 3,
      "node_id": "MDEwOlJlcG9zaXRvcnkxMjk2MjY3",
      "name": "React + Next",
      "full_name": "horong/template-react-next",
      "private": true,
      "html_url": "https://github.com/horong/horong/template-react-next",
      "description": "This your first repo!",
    },
  ]));
}
function* fetchGithubBranches({ repo_id }) {
  yield put(actions.setValue('githubBranches', [
    {
      "id":1,
      "name": "master",
      "commit": {
        "sha": "c5b97d5ae6c19d5c5df71a34c7fbeeda2479ccbc",
        "url": "https://api.github.com/repos/octocat/Hello-World/commits/c5b97d5ae6c19d5c5df71a34c7fbeeda2479ccbc"
      },
      "protected": true,
      "protection": {
        "enabled": true,
        "required_status_checks": {
          "enforcement_level": "non_admins",
          "contexts": [
            "ci-test",
            "linter"
          ]
        }
      },
      "protection_url": "https://api.github.com/repos/octocat/hello-world/branches/master/protection"
    },
    {
      "id":2,
      "name": "staging",
      "commit": {
        "sha": "c5b97d5ae6c19d5c5df71a34c7fbeeda2479ccbc",
        "url": "https://api.github.com/repos/octocat/Hello-World/commits/c5b97d5ae6c19d5c5df71a34c7fbeeda2479ccbc"
      },
      "protected": true,
      "protection": {
        "enabled": true,
        "required_status_checks": {
          "enforcement_level": "non_admins",
          "contexts": [
            "ci-test",
            "linter"
          ]
        }
      },
      "protection_url": "https://api.github.com/repos/octocat/hello-world/branches/master/protection"
    },
  ]));
}
function* fetchGithubIssues({ repo_id }) {
  yield put(actions.setValue('githubIssues', [
    {
      "id": 1,
      "node_id": "MDU6SXNzdWUx",
      "html_url": "https://github.com/octocat/Hello-World/issues/1347",
      "number": 1347,
      "state": "open",
      "title": "Found a bug",
      "body": "I'm having a problem with this.",
    },
    {
      "id": 2,
      "node_id": "MDU6SXNzdWUx",
      "html_url": "https://github.com/octocat/Hello-World/issues/1347",
      "number": 1347,
      "state": "open",
      "title": "Found a bug",
      "body": "I'm having a problem with this.",
    },
    {
      "id": 3,
      "node_id": "MDU6SXNzdWUx",
      "html_url": "https://github.com/octocat/Hello-World/issues/1347",
      "number": 1347,
      "state": "open",
      "title": "Found a bug",
      "body": "I'm having a problem with this.",
    },
  ]));
}
function* fetchGithubPullRequest({ repo_id }) {
  yield put(actions.setValue('githubPullRequests', [
    {
      "id": 1,
      "node_id": "MDExOlB1bGxSZXF1ZXN0MQ==",
      "html_url": "https://github.com/octocat/Hello-World/pull/1347",
      "number": 1347,
      "state": "open",
      "locked": true,
      "title": "Amazing new feature",
      "user": {
        "login": "octocat",
      },
      "body": "Please pull these awesome changes in!",
    },
    {
      "id": 2,
      "node_id": "MDExOlB1bGxSZXF1ZXN0MQ==",
      "html_url": "https://github.com/octocat/Hello-World/pull/1347",
      "number": 1347,
      "state": "open",
      "locked": true,
      "title": "Amazing new feature",
      "user": {
        "login": "octocat",
      },
      "body": "Please pull these awesome changes in!",
    },
    {
      "id": 3,
      "node_id": "MDExOlB1bGxSZXF1ZXN0MQ==",
      "html_url": "https://github.com/octocat/Hello-World/pull/1347",
      "number": 1347,
      "state": "open",
      "locked": true,
      "title": "Amazing new feature",
      "user": {
        "login": "octocat",
      },
      "body": "Please pull these awesome changes in!",
    }
  ]));
}

function* fetchGitlabRepositories({ user_id }) {
  yield put(actions.setValue('gitlabRepositories', [
    {
      "id": 1,
      "description": null,
      "default_branch": "master",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
      "web_url": "http://example.com/diaspora/diaspora-client",
      "readme_url": "http://example.com/diaspora/diaspora-client/blob/master/README.md",
      "tag_list": [
        "example",
        "disapora client"
      ],
      "name": "Diaspora Client",
      "name_with_namespace": "Diaspora / Diaspora Client",
      "path": "diaspora-client",
      "path_with_namespace": "diaspora/diaspora-client",
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "forks_count": 0,
      "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
      "star_count": 0,
    },
    {
      "id": 2,
      "description": null,
      "default_branch": "master",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
      "web_url": "http://example.com/diaspora/diaspora-client",
      "readme_url": "http://example.com/diaspora/diaspora-client/blob/master/README.md",
      "tag_list": [
        "example",
        "disapora client"
      ],
      "name": "Diaspora Client",
      "name_with_namespace": "Diaspora / Diaspora Client",
      "path": "diaspora-client",
      "path_with_namespace": "diaspora/diaspora-client",
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "forks_count": 0,
      "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
      "star_count": 0,
    },
    {
      "id": 3,
      "description": null,
      "default_branch": "master",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
      "web_url": "http://example.com/diaspora/diaspora-client",
      "readme_url": "http://example.com/diaspora/diaspora-client/blob/master/README.md",
      "tag_list": [
        "example",
        "disapora client"
      ],
      "name": "Diaspora Client",
      "name_with_namespace": "Diaspora / Diaspora Client",
      "path": "diaspora-client",
      "path_with_namespace": "diaspora/diaspora-client",
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "forks_count": 0,
      "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
      "star_count": 0,
    },
  ]));
}
function* fetchGitlabBranches({ repo_id }) {
  yield put(actions.setValue('gitlabBranches', [
    {
      "id":1,
      "name": "master",
      "merged": false,
      "protected": true,
      "default": true,
      "developers_can_push": false,
      "developers_can_merge": false,
      "can_push": true,
      "web_url": "http://gitlab.example.com/my-group/my-project/-/tree/master",
      "commit": {
        "author_email": "john@example.com",
        "author_name": "John Smith",
        "authored_date": "2012-06-27T05:51:39-07:00",
        "committed_date": "2012-06-28T03:44:20-07:00",
        "committer_email": "john@example.com",
        "committer_name": "John Smith",
        "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
        "short_id": "7b5c3cc",
        "title": "add projects API",
        "message": "add projects API",
        "parent_ids": [
          "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
        ]
      }
    },
    {
      "id":2,
      "name": "staging",
      "merged": false,
      "protected": true,
      "default": true,
      "developers_can_push": false,
      "developers_can_merge": false,
      "can_push": true,
      "web_url": "http://gitlab.example.com/my-group/my-project/-/tree/master",
      "commit": {
        "author_email": "john@example.com",
        "author_name": "John Smith",
        "authored_date": "2012-06-27T05:51:39-07:00",
        "committed_date": "2012-06-28T03:44:20-07:00",
        "committer_email": "john@example.com",
        "committer_name": "John Smith",
        "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
        "short_id": "7b5c3cc",
        "title": "add projects API",
        "message": "add projects API",
        "parent_ids": [
          "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
        ]
      }
    }
  ]));
}
function* fetchGitlabIssues({ repo_id }) {
  yield put(actions.setValue('gitlabIssues', [
    {
      "id" : 1,
      "state" : "opened",
      "description" : "Ratione dolores corrupti mollitia soluta quia.",
      "author" : {
         "state" : "active",
         "id" : 18,
         "name" : "Alexandra Bashirian",
         "username" : "eileen.lowe"
      },
      "updated_at" : "2016-01-04T15:31:51.081Z",
      "title" : "Consequatur vero maxime deserunt laboriosam est voluptas dolorem.",
      "created_at" : "2016-01-04T15:31:51.081Z",
      "due_date": "2016-07-22",
      "web_url": "http://gitlab.example.com/my-group/my-project/issues/6",
    },
    {
      "id" : 2,
      "state" : "opened",
      "description" : "Ratione dolores corrupti mollitia soluta quia.",
      "author" : {
         "state" : "active",
         "id" : 18,
         "name" : "Alexandra Bashirian",
         "username" : "eileen.lowe"
      },
      "updated_at" : "2016-01-04T15:31:51.081Z",
      "title" : "Consequatur vero maxime deserunt laboriosam est voluptas dolorem.",
      "created_at" : "2016-01-04T15:31:51.081Z",
      "due_date": "2016-07-22",
      "web_url": "http://gitlab.example.com/my-group/my-project/issues/6",
    },
    {
      "id" : 3,
      "state" : "opened",
      "description" : "Ratione dolores corrupti mollitia soluta quia.",
      "author" : {
         "state" : "active",
         "id" : 18,
         "name" : "Alexandra Bashirian",
         "username" : "eileen.lowe"
      },
      "updated_at" : "2016-01-04T15:31:51.081Z",
      "title" : "Consequatur vero maxime deserunt laboriosam est voluptas dolorem.",
      "created_at" : "2016-01-04T15:31:51.081Z",
      "due_date": "2016-07-22",
      "web_url": "http://gitlab.example.com/my-group/my-project/issues/6",
    },
  ]));
}
function* fetchGitlabMergeRequest({ repo_id }) {
  yield put(actions.setValue('gitlabMergeRequests', [
    {
      "id": 1,
      "iid": 1,
      "project_id": 3,
      "title": "test1",
      "description": "fixed login page css paddings",
      "state": "merged",
      "merged_by": {
        "id": 87854,
        "name": "Douwe Maan",
        "username": "DouweM",
        "state": "active",
        "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
        "web_url": "https://gitlab.com/DouweM"
      },
      "merged_at": "2018-09-07T11:16:17.520Z",
      "closed_by": null,
      "closed_at": null,
      "created_at": "2017-04-29T08:46:00Z",
      "updated_at": "2017-04-29T08:46:00Z",
      "target_branch": "master",
      "source_branch": "test1",
      "upvotes": 0,
      "downvotes": 0,
      "author": {
        "id": 1,
        "name": "Administrator",
        "username": "admin",
        "state": "active",
        "avatar_url": null,
        "web_url" : "https://gitlab.example.com/admin"
      },
      "source_project_id": 2,
      "target_project_id": 3,
      "labels": [
        "Community contribution",
        "Manage"
      ],
      "merge_status": "can_be_merged",
    },
    {
      "id": 2,
      "iid": 1,
      "project_id": 3,
      "title": "test1",
      "description": "fixed login page css paddings",
      "state": "merged",
      "merged_by": {
        "id": 87854,
        "name": "Douwe Maan",
        "username": "DouweM",
        "state": "active",
        "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
        "web_url": "https://gitlab.com/DouweM"
      },
      "merged_at": "2018-09-07T11:16:17.520Z",
      "closed_by": null,
      "closed_at": null,
      "created_at": "2017-04-29T08:46:00Z",
      "updated_at": "2017-04-29T08:46:00Z",
      "target_branch": "master",
      "source_branch": "test1",
      "upvotes": 0,
      "downvotes": 0,
      "author": {
        "id": 1,
        "name": "Administrator",
        "username": "admin",
        "state": "active",
        "avatar_url": null,
        "web_url" : "https://gitlab.example.com/admin"
      },
      "source_project_id": 2,
      "target_project_id": 3,
      "labels": [
        "Community contribution",
        "Manage"
      ],
      "merge_status": "can_be_merged",
    },
    {
      "id": 3,
      "iid": 1,
      "project_id": 3,
      "title": "test1",
      "description": "fixed login page css paddings",
      "state": "merged",
      "merged_by": {
        "id": 87854,
        "name": "Douwe Maan",
        "username": "DouweM",
        "state": "active",
        "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
        "web_url": "https://gitlab.com/DouweM"
      },
      "merged_at": "2018-09-07T11:16:17.520Z",
      "closed_by": null,
      "closed_at": null,
      "created_at": "2017-04-29T08:46:00Z",
      "updated_at": "2017-04-29T08:46:00Z",
      "target_branch": "master",
      "source_branch": "test1",
      "upvotes": 0,
      "downvotes": 0,
      "author": {
        "id": 1,
        "name": "Administrator",
        "username": "admin",
        "state": "active",
        "avatar_url": null,
        "web_url" : "https://gitlab.example.com/admin"
      },
      "source_project_id": 2,
      "target_project_id": 3,
      "labels": [
        "Community contribution",
        "Manage"
      ],
      "merge_status": "can_be_merged",
    },
  ]));
}

export default function* () {
  yield all([
    takeLeading(Types.FETCH_GITHUB_REPOSITORIES, fetchGithubRepositories),
    takeLeading(Types.FETCH_GITHUB_BRANCHES, fetchGithubBranches),
    takeLeading(Types.FETCH_GITHUB_ISSUES, fetchGithubIssues),
    takeLeading(Types.FETCH_GITHUB_PULL_REQUEST, fetchGithubPullRequest),

    takeLeading(Types.FETCH_GITLAB_REPOSITORIES, fetchGitlabRepositories),
    takeLeading(Types.FETCH_GITLAB_BRANCHES, fetchGitlabBranches),
    takeLeading(Types.FETCH_GITLAB_ISSUES, fetchGitlabIssues),
    takeLeading(Types.FETCH_GITLAB_MERGE_REQUEST, fetchGitlabMergeRequest),
  ]);
}
