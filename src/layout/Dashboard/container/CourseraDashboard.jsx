import React, { useEffect } from 'react';
import { MdDashboard, MdFormatListBulleted, MdSentimentSatisfied } from 'react-icons/md';
import { GiGluttonousSmile } from 'react-icons/gi';
import { AiFillAliwangwang } from 'react-icons/ai';

export default function CourseraDashboard() {
  const url = `https://www.coursera.org/`;

  useEffect(() => {
    setEvent();
  }, [])

  function setEvent (){
    document.querySelectorAll('.card-view').forEach(view => {
      view.classList.add('on')
    })

    document.querySelectorAll('.tab').forEach(tab => {
      tab.addEventListener('click', function(e){
        if(document.querySelector('.tab.on')){
          document.querySelector('.tab.on').classList.remove('on');
        }
        e.currentTarget.className = 'tab on';

        if(document.querySelector('.tab-content.on')){
          document.querySelector('.tab-content.on').classList.remove('on');
        }
        const target = e.currentTarget.getAttribute("data-target");
        document.getElementById(target).classList.add('on');
      })
    })
    document.querySelectorAll('.icon-tab').forEach(tab => {
      tab.addEventListener('click', function(e){
        if(document.querySelector('.icon-tab.on')){
          document.querySelector('.icon-tab.on').classList.remove('on');
        }
        e.currentTarget.className = 'icon-tab on';

        const view = e.currentTarget.getAttribute("data-view");

        if (view === 'card') {
          document.querySelectorAll('.table-view').forEach(view => {
            view.classList.remove('on')
          })
          document.querySelectorAll('.card-view').forEach(view => {
            view.classList.add('on')
          })
        } else if (view === 'table') {
          document.querySelectorAll('.card-view').forEach(view => {
            view.classList.remove('on')
          })
          document.querySelectorAll('.table-view').forEach(view => {
            view.classList.add('on')
          })
        }
      })
      
    })
  }

  return (
    <div id="coursera" className="coursera clear">
      <section className="b-contents">
        <div className="b-dashboard">
          <header className="tab-box">
            <div className="group">
              <a data-target="all" className="tab on">All</a>
              <a data-target="machine-learning" className="tab">In Progress</a>
              <a data-target="data-science" className="tab">Done</a>
            </div>
            <div className="group icon">
              <a data-view="card" className="icon-tab on"><MdDashboard/></a>
              <a data-view="table" className="icon-tab"><MdFormatListBulleted/></a>
            </div>
          </header>
          <section className="tab-container">
            <div className="tab-content clear on" id="all">
              <div className="table-view">
                <div className="table">
                  <table>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>University</th>
                        <th>Title</th>
                        <th>Progress</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].map((e, i) => (
                          <tr key={i}>
                            <td>{i+1}</td>
                            <td>UNIVERSITY OF MINNESOTA</td>
                            <td>Earn your UX design certificate in just 6 months</td>
                            <td>10/24</td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="card-view">
              {
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].map((e, i) => (
                  <div className="card" key={i}>
                    <span className="card-university">UNIVERSITY OF MINNESOTA</span>
                    <h3 className="card-title">Earn your UX design certificate in just 6 months</h3>
                    <p className="card-description">Apply design theories to your next project with the UX Design MasterTrack Certificate.</p>
                    <div className="card-status">
                      <div className="progressbar">
                        <div className="fill">
                          <span>7</span>
                          <span>/</span>
                          <span>10</span>
                          <GiGluttonousSmile />
                        </div>
                      </div>
                    </div>
                    <div className="tags">
                      <span>json</span>
                      <span>xml</span>
                      <span>SQL</span>
                      <span>Database</span>
                    </div>
                  </div>
                ))
              }
              </div>
            </div>
            <div className="tab-content clear" id="machine-learning">
              <div className="table-view">
                <div className="table">
                  <table>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>University</th>
                        <th>Title</th>
                        <th>Progress</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].map((e, i) => (
                          <tr key={i}>
                            <td>{i+1}</td>
                            <td>UNIVERSITY OF MINNESOTA</td>
                            <td>Earn your UX design certificate in just 6 months</td>
                            <td>10/24</td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="card-view">
              {
                [0,0,0,0,0,0].map((e, i) => (
                  <div className="card" key={i}>
                    <span className="card-university">UNIVERSITY OF MINNESOTA</span>
                    <h3 className="card-title">Earn your UX design certificate in just 6 months</h3>
                    <p className="card-description">Apply design theories to your next project with the UX Design MasterTrack Certificate.</p>
                    <div className="card-status">
                      <div className="progressbar">
                        <div className="fill">
                          <span>7</span>
                          <span>/</span>
                          <span>10</span>
                          <AiFillAliwangwang />
                        </div>
                      </div>
                    </div>
                    <div className="tags">
                      <span>json</span>
                      <span>xml</span>
                      <span>SQL</span>
                      <span>Database</span>
                    </div>
                  </div>
                ))
              }
              </div>
            </div>
            <div className="tab-content clear" id="data-science">
              <div className="table-view">
                <div className="table">
                  <table>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>University</th>
                        <th>Title</th>
                        <th>Progress</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].map((e, i) => (
                          <tr key={i}>
                            <td>{i+1}</td>
                            <td>UNIVERSITY OF MINNESOTA</td>
                            <td>Earn your UX design certificate in just 6 months</td>
                            <td>10/24</td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="card-view">
              {
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0].map((e, i) => (
                  <div className="card" key={i}>
                    <span className="card-university">UNIVERSITY OF MINNESOTA</span>
                    <h3 className="card-title">Earn your UX design certificate in just 6 months</h3>
                    <p className="card-description">Apply design theories to your next project with the UX Design MasterTrack Certificate.</p>
                    <div className="card-status">
                      <div className="progressbar">
                        <div className="fill">
                          <span>7</span>
                          <span>/</span>
                          <span>10</span>
                          <MdSentimentSatisfied />
                        </div>
                      </div>
                    </div>
                    <div className="tags">
                      <span>json</span>
                      <span>xml</span>
                      <span>SQL</span>
                      <span>Database</span>
                    </div>
                  </div>
                ))
              }
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  )
}