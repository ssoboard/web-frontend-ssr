import React from 'react';
import { ImCheckboxChecked, ImCheckboxUnchecked } from 'react-icons/im';
import Highlight from 'react-highlight';

export default function CodingTestDashboard() {

  function toggleLanguage(){
    const isOn = document.querySelector('.language .list').classList.contains('on');
    if(isOn) {
      document.querySelector('.language .list').classList.remove('on');
    } else {
      document.querySelector('.language .list').classList.add('on');
    }
  }

  function selectLanguage(e){
    const value = e.currentTarget.innerHTML;
    document.querySelector('.language .value').innerHTML = value;
    toggleLanguage()
  }

  function selectLevel(e){
    if(document.querySelector('.item.on')) {
      document.querySelector('.item.on').classList.remove('on');
    }
    e.currentTarget.className = 'item on';
  }

  function check(e){
  }

  return (
    <div id="codingtest" className="codingtest clear">
      <div className="menu">
        <div className="language">
          <div className="value" onClick={toggleLanguage}>Javascript</div>
          <ul className="list">
            <li onClick={selectLanguage} className="item">Javascript</li>
            <li onClick={selectLanguage} className="item">Python</li>
            <li onClick={selectLanguage} className="item">Ruby</li>
            <li onClick={selectLanguage} className="item">SQL</li>
          </ul>
        </div>

        <div className="test">
          <h2 className="sub-title">Programmers</h2>
          <div className="group">
            <a className="level" href="#">Level 1 (0/3)</a>
            <ul className="list">
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">크레인 인형뽑기 게임</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">레코드 조회하기</a>
              </li>
            </ul>
          </div>
          <div className="group">
            <a className="level" href="#">Level 2 (2/4)</a>
            <ul className="list">
              <li className="item on" onClick={selectLevel}>
                <ImCheckboxChecked className="checked" />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxChecked className="checked" />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
            </ul>
          </div>
          <div className="group">
            <a className="level" href="#">Level 3 (0/3)</a>
            <ul className="list">
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
              <li className="item clear" onClick={selectLevel}>
                <ImCheckboxUnchecked />
                <a className="i-title">평균값 구하기</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="code">
        <Highlight language="javascript">
          {codeString2}
        </Highlight>
      </div>
    </div>
  )
}

const codeString2 = 
`import React from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vscDarkPlus } from 'react-syntax-highlighter/dist/esm/styles/prism';

const initialState = {
  // Github
  githubRepositories:[],
  githubBranches:[],
  githubIssues:[], 
  githubPullRequests:[],

  // Gitlab
  gitlabRepositories:[],
  gitlabBranches:[],
  gitlabIssues:[],
  gitlabMergeRequests:[],
};

const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});
const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});
const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});
const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});
const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});
const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});
`;
