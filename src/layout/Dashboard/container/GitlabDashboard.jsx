import React, { useEffect, useState } from 'react'

// Components
import KanbanBoard from '../component/KanbanBoard';

// Icons
import { BiGitBranch, BiGitPullRequest } from 'react-icons/bi';
import { VscIssues } from 'react-icons/vsc';
import { RiGitRepositoryLine } from 'react-icons/ri';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../state';

export default function GitLabDashboard() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchGitlabRepositories('user_id'));
  }, [dispatch]);

  const gitlabRepositories = useSelector(state => state.dashboard.gitlabRepositories);
  const gitlabBranches = useSelector(state => state.dashboard.gitlabBranches);
  const gitlabIssues = useSelector(state => state.dashboard.gitlabIssues);
  const gitlabMergeRequests = useSelector(state => state.dashboard.gitlabMergeRequests);

  function onClickRepository (repo_id) {
    dispatch(actions.fetchGitlabRepositories(repo_id));
    dispatch(actions.fetchGitlabBranches(repo_id));
    dispatch(actions.fetchGitlabIssues(repo_id));
    dispatch(actions.fetchGitlabMergeRequest(repo_id));
  }
  
  return (
    <div id="gitlab" className="clear">
      <KanbanBoard 
        className="repositories"
        Icon={RiGitRepositoryLine}
        title="Repositories"
        emptyText="No repositories"
        dataList={gitlabRepositories}
        onClickCard={onClickRepository}
      />
      <KanbanBoard 
        className="branches"
        Icon={BiGitBranch}
        title="Branches"
        emptyText="No branches"
        dataList={gitlabBranches}
      />
      <KanbanBoard 
        className="issues"
        Icon={VscIssues}
        title="Issues"
        emptyText="No issues"
        dataList={gitlabIssues}
      />
      <KanbanBoard
        className="merge-requests"
        Icon={BiGitPullRequest}
        title="Merge Requests"
        emptyText="No merge requests"
        dataList={gitlabMergeRequests}
      />
    </div>
  )
}