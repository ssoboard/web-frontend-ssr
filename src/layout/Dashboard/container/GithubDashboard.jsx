import React, { useEffect, useState } from 'react'

// Components
import KanbanBoard from '../component/KanbanBoard';

// Icons
import { BiGitBranch, BiGitPullRequest } from 'react-icons/bi';
import { VscIssues } from 'react-icons/vsc';
import { RiGitRepositoryLine } from 'react-icons/ri';

import { actions } from '../state';
import { useDispatch, useSelector } from 'react-redux';

export default function GitHubDashboard() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchGithubRepositories('user_id'));
  }, [dispatch]);

  const githubRepositories = useSelector(state => state.dashboard.githubRepositories);
  const githubBranches = useSelector(state => state.dashboard.githubBranches);
  const githubIssues = useSelector(state => state.dashboard.githubIssues);
  const githubPullRequests = useSelector(state => state.dashboard.githubPullRequests);

  function onClickRepository (repo_id) {
    dispatch(actions.fetchGithubRepositories(repo_id));
    dispatch(actions.fetchGithubBranches(repo_id));
    dispatch(actions.fetchGithubIssues(repo_id));
    dispatch(actions.fetchGithubPullRequest(repo_id));
  }
  
  return (
    <div id="github" className="clear">
      <KanbanBoard 
        className="repositories"
        Icon={RiGitRepositoryLine}
        title="Repositories"
        emptyText="No repositories"
        dataList={githubRepositories}
        onClickCard={onClickRepository}
      />
      <KanbanBoard 
        className="branches"
        Icon={BiGitBranch}
        title="Branches"
        emptyText="No branches"
        dataList={githubBranches}
      />
      <KanbanBoard 
        className="issues"
        Icon={VscIssues}
        title="Issues"
        emptyText="No issues"
        dataList={githubIssues}
      />
      <KanbanBoard
        className="pull-requests"
        Icon={BiGitPullRequest}
        title="Pull Requests"
        emptyText="No pull requests"
        dataList={githubPullRequests}
      />
    </div>
  )
}