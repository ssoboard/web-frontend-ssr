import React from 'react'
import { BiLinkExternal } from 'react-icons/bi';
import { SiBabel, SiReact, SiNodeDotJs } from 'react-icons/si';
import { AiFillLock, AiFillUnlock } from 'react-icons/ai';


export default function KanbanBoard ({ 
  className, 
  dataList, 
  title, 
  emptyText="No data", 
  onClickCard=function(e){}
}) {
  return (
    <div className={"clear kanban " + className}>
      <h2 className="title">{title}</h2>
      <ul>
        {dataList.length === 0 
         ? <li className="card empty">{emptyText}</li>
         : <></>
        }
        {dataList.map(data => (
        <li key={data.id} className="card repository clear" onClick={() => {onClickCard(data.id)}}>
          {/* <a className="link" href={data.html_url || data.web_url}><BiLinkExternal/></a> */}
          <div className="card-content">
            <h3 className="sub-title">{data.name || data.title}</h3>
          </div>
        </li>
        ))}
      </ul>
    </div>
  );
}