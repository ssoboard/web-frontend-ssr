import {
  createReducer,
  createSetValueAction,
  setValueReducer,
  NOT_IMMUTABLE,
} from '../../Common/redux-helper';

export const Types = {
  SET_VALUE: 'header/SET_VALUE',
  FETCH_NOTIFICATIONS: 'header/FETCH_NOTIFICATIONS',
  READ_NOTIFICATIONS: 'header/READ_NOTIFICATIONS',
  INITIALIZE: 'header/INITIALIZE',
};

export const actions = {
  // ReduxActions
  setValue: createSetValueAction(Types.SET_VALUE),
  initialize: () => ({ type: Types.INITIALIZE, [NOT_IMMUTABLE]: true }),

  // SagaActions
  fetchNotifications: () => ({
    type: Types.FETCH_NOTIFICATIONS
  }),
  readNotification: (id) => ({
    type: Types.READ_NOTIFICATIONS,
    id
  })
};

const initialState = {
  notifications: [],
  isShowNotificationDropdown: false,
  isNewNotifications: false,

  isShowProfileDropdown: false,
  
  searchKeyword: '',
};

const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});

export default reducer;
