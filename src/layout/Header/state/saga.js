import { all, call, put, takeLeading } from 'redux-saga/effects';
import { Types, actions } from '.';

function* fetchNotifications() {
  yield put(actions.setValue('notifications', [
    {id: '1', type: 'normal', isNew: true,  title: 'Server Error Reports', content: 'This is error reports. Critical Issue.', time: '3 minutes ago'},
    {id: '2', type: 'normal', isNew: true,  title: 'Server Error Reports', content: 'This is error reports. Critical Issue.', time: '3 minutes ago'},
    {id: '3', type: 'normal', isNew: false, title: 'Server Error Reports', content: 'This is error reports. Critical Issue.', time: '3 minutes ago'},
    {id: '4', type: 'normal', isNew: false, title: 'Server Error Reports', content: 'This is error reports. Critical Issue.', time: '3 minutes ago'},
    {id: '5', type: 'normal', isNew: false, title: 'Server Error Reports', content: 'This is error reports. Critical Issue.', time: '3 minutes ago'},
  ]));
}

export default function* () {
  yield all([
    takeLeading(
      Types.FETCH_NOTIFICATIONS,
      fetchNotifications
    ),
  ]);
}
