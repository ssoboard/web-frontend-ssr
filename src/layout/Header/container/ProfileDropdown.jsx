import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { RiArrowDownSLine } from 'react-icons/ri';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../state';

export default function ProfileDropdown() {
  // Dispatch
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.setValue('name', localStorage.getItem('name')));
    document.querySelector('#__next').addEventListener('click', close);
    return document.body.removeEventListener('click', close);
  }, [dispatch])
  
  // State
  const isShowProfileDropdown = useSelector(state => state.header.isShowProfileDropdown);
  const name = useSelector(state => state.header.name);

  // Event
  function toggle(){
    dispatch(actions.setValue('isShowNotificationDropdown', false));
    dispatch(actions.setValue('isShowProfileDropdown', !isShowProfileDropdown));
  }
  function signout(){
    localStorage.removeItem('name');
    dispatch(actions.setValue('name', ''));
  }
  function close(){
    dispatch(actions.setValue('isShowNotificationDropdown', false));
    dispatch(actions.setValue('isShowProfileDropdown', false));
  }

  return (
    <div className="dropdown">
      <a className="dropdown-target" onClick={toggle}>
        <div className="image">
          <img src="/profile.jpg" alt="프로필 사진" />
          <span>{name}</span>
          <RiArrowDownSLine />
        </div>
      </a>
      <div className={isShowProfileDropdown ? "list on" : "list"}>
        <div className="item"><Link href="/"><a>Edit Profile</a></Link></div>
        <div className="item"><Link href="/"><a>Setting</a></Link></div>
        <div className="divider"></div>
        <div className="item"><Link href="/auth/signin"><a onClick={signout}>Sign Out</a></Link></div>
      </div>
    </div>
  )
}