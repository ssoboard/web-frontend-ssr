import React from 'react';
import { IoMdSearch } from 'react-icons/io';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../state';

export default function SearchForm() {
  const dispatch = useDispatch();
  const searchKeyword = useSelector(state => state.header.searchKeyword);

  function onChange(e) {
    const newValue = e.currentTarget.value || '';
    dispatch(actions.setValue('searchKeyword', newValue));
  }

  return (
    <form className="search">
      <input type="text" placeholder="검색" onChange={onChange} value={searchKeyword}/>
      <IoMdSearch />
    </form>
  );
}
