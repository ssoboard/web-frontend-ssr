import React, { useEffect, useState } from 'react';
import { VscBellDot, VscBell } from 'react-icons/vsc';
import { FaGg } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../state';

export default function NotificationDropdown() {
  
  // Dispatch
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchNotifications());
    const interval = setInterval(()=>{
      dispatch(actions.fetchNotifications());
    }, 60*1000);
    return function(){
      clearInterval(interval);
    };
  }, [dispatch])

  // State
  const notifications = useSelector(state => state.header.notifications);
  const isShowNotificationDropdown = useSelector(state => state.header.isShowNotificationDropdown);
  const isNewNotifications = useSelector(state => state.header.isNewNotifications)

  const isExist = notifications !== undefined;
  const hasNew = notifications.filter((notification) => notification.isNew === true).length > 0;
  if(isExist && hasNew){
    dispatch(actions.setValue('isNewNotifications', true));
  }

  // Event
  function toggle(){
    dispatch(actions.fetchNotifications());
    dispatch(actions.setValue('isShowProfileDropdown', false));
    dispatch(actions.setValue('isShowNotificationDropdown', !isShowNotificationDropdown));
  }
  function readNotification(){
    if(isShowNotificationDropdown){
      dispatch(actions.setValue('isShowNotificationDropdown', false));
    }
  }

  // Rendering
  console.log('Rendering - NotificationDropdown');
  return (
    <div className="dropdown">
      <a className="dropdown-target" onClick={toggle}>
        {isNewNotifications ? <VscBellDot /> : <VscBell />}
      </a>
      <div className={isShowNotificationDropdown ? "list on" : "list"}>
        <h2 className="notification-header">notifications</h2>
        {notifications.map((notification) => (
          <div className={notification.isNew ? "notification clear new" : "notification clear"} key={notification.id} onClick={readNotification}>
            <div className="icon">
              <FaGg />
            </div>
            <div className="body">
              <h6 className="n-title">{notification.title}</h6>
              <p className="n-content">{notification.content}</p>
              <div className="n-time">{notification.time}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}