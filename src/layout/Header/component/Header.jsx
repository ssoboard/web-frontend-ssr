import React from 'react';
import Logo from './Logo';
import NotificationDropdown from '../container/NotificationDropdown';
import ProfileDropdown from '../container/ProfileDropdown';
import SearchForm from '../container/SearchForm';

export default function Header() {
  return (
    <header id="header" className="header">
      <Logo />
      <div className="profile">
        <SearchForm />
        <NotificationDropdown />
        <ProfileDropdown />
      </div>
    </header>
  );
}