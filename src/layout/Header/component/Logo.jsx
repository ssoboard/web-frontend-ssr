import Link from 'next/link';
import React from 'react';
import { FaGg } from 'react-icons/fa';
import Router from 'next/router';

export default function Logo() {
  return (
    <h1 className="logo" onClick={()=>{Router.push('/')}}>
      <FaGg />
      <span className="text">S. Board</span>
    </h1>
  );
}
