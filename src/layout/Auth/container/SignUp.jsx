import Link from 'next/link';
import React, { useEffect } from 'react';
import { FaGg } from 'react-icons/fa';
import { ImFacebook, ImGoogle, ImLinkedin2, ImTwitter } from 'react-icons/im';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../state';
import Router from 'next/router';
import Axios from 'axios';

function SignUp() {
  // State
  const dispatch = useDispatch();
  const { socialUrlGoogle, socialUrlFacebook, socialUrlTwitter, socialUrlLinkedin } = useSelector(state => state.auth);
  const { email, password, confirmPassword } = useSelector(state => state.auth);

  // Event
  function onChangeInput(e, key) {
    const newValue = e.currentTarget.value;
    dispatch(actions.setValue(key, newValue));
  }
  function onChangeEmail (e) {onChangeInput(e, 'email');}
  function onChangePassword (e) {onChangeInput(e, 'password');}
  function onChangeConfirmPassword (e) {onChangeInput(e, 'confirmPassword');}
  function onSubmit () {
    Axios
    .post('http://localhost:3300/auth/signup', { email, password })
    .then(data => {
      Router.push('/auth/signin');
    });
  }

  // Rendering
  return (
    <div className="account">
      <div className="container">
        <h1 className="logo clear">
          <FaGg />
          <span className="text">S. Board</span>
        </h1>
        <form className="form">
          <input onChange={onChangeEmail} value={email} className="email" type="text" placeholder="Email"/>
          <input onChange={onChangePassword} value={password} className="password" type="password" placeholder="Password"/>
          <input onChange={onChangeConfirmPassword} value={confirmPassword} className="confirmPassword" type="password" placeholder="Confirm Password"/>
          <button className="button" type="button" onClick={onSubmit}>Sign Up</button>
          <p className="or-with">Or With</p>
          <div className="social">
            <Link href={socialUrlGoogle}><a className="item"><ImGoogle/></a></Link>
            <Link href={socialUrlFacebook}><a className="item"><ImFacebook/></a></Link>
            <Link href={socialUrlTwitter}><a className="item"><ImTwitter/></a></Link>
            <Link href={socialUrlLinkedin}><a className="item"><ImLinkedin2/></a></Link>
          </div>
        </form>
        <p className="comment">
          <span>Already have an account?</span>
          <Link href="/auth/signin">
            <a>Sign In</a>
          </Link>
        </p>
      </div>
    </div>
  );
}

export default SignUp;
