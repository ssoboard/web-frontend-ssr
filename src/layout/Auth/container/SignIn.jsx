import React from 'react';
import Link from 'next/link';
import { FaGg } from 'react-icons/fa';
import { ImFacebook, ImGoogle, ImLinkedin2, ImTwitter } from 'react-icons/im';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../state/index';
import Router from 'next/router';
import Axios from 'axios';

export default function SignIn() {
  // State
  const dispatch = useDispatch();
  const { socialUrlGoogle, socialUrlFacebook, socialUrlTwitter, socialUrlLinkedin } = useSelector(state => state.auth);
  const { email, password } = useSelector(state => state.auth);

  // Event
  function onChangeInput(e, key) {
    const newValue = e.currentTarget.value;
    dispatch(actions.setValue(key, newValue));
  }
  function onChangeEmail (e) {onChangeInput(e, 'email');}
  function onChangePassword (e) {onChangeInput(e, 'password');}
  function onSubmit () {
    Axios.post('http://localhost:3300/auth/signin', { email, password }, { withCredentials: true })
    .then(data => console.log(data))
  }

  function checkCookie () {
    Axios.get('http://localhost:3300/cookieCheck', { withCredentials: true })
    .then(data => console.log(data))
  }

  // Rendering
  return (
    <div className="account">
      <div className="container">
        <h1 className="logo clear">
          <FaGg />
          <span className="text">S. Board</span>
        </h1>
        <form className="form">
          <input onChange={onChangeEmail} value={email} className="email" type="text" placeholder="Email"/>
          <input onChange={onChangePassword} value={password} className="password" type="password" placeholder="Password"/>
          <a href="#" className="forgot">Forgot Password?</a>
          <button className="button" type="button" onClick={onSubmit}>Sign In</button>
          <button className="button" type="button" onClick={checkCookie}>checkCookie</button>
          <p className="or-with">Or With</p>
          <div className="social">
            <Link href={socialUrlGoogle}><a className="item"><ImGoogle/></a></Link>
            <Link href={socialUrlFacebook}><a className="item"><ImFacebook/></a></Link>
            <Link href={socialUrlTwitter}><a className="item"><ImTwitter/></a></Link>
            <Link href={socialUrlLinkedin}><a className="item"><ImLinkedin2/></a></Link>
          </div>
        </form>
        <p className="comment">
          <span>Don't have an account?</span>
          <Link href="/auth/signup">
            <a>Sign Up</a>
          </Link>
        </p>
      </div>
    </div>
  );
}