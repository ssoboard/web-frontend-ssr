import {
  createReducer,
  createSetValueAction,
  setValueReducer,
  NOT_IMMUTABLE,
} from '../../Common/redux-helper';

export const Types = {
  SET_VALUE: 'auth/SET_VALUE',
  FETCH_SOCIAL_URL: 'auth/FETCH_SOCIAL_URL',
  FETCH_SIGN_IN: 'auth/FETCH_SIGN_IN',
  FETCH_SIGN_UP: 'auth/FETCH_SIGN_UP',
  INITIALIZE: 'auth/INITIALIZE',
};

export const actions = {
  // ReduxActions
  setValue: createSetValueAction(Types.SET_VALUE),
  initialize: () => ({ type: Types.INITIALIZE, [NOT_IMMUTABLE]: true }),

  // SagaActions
  fetchSocialUrl: () => ({
    type: Types.FETCH_SOCIAL_URL
  }),
  fetchSignIn: ( id, password, ) => ({
    type: Types.FETCH_SIGN_IN,
    id, password,
  }),
  fetchSignUp: ( id, password, confirmPassword, email, ) => ({
    type: Types.FETCH_SIGN_UP,
    id, password, confirmPassword, email,
  })
};

const initialState = {
  id:'',
  password:'',
  confirmPassword:'',
  email:'',
  
  socialUrlGoogle:'',
  socialUrlFacebook:'',
  socialUrlTwitter:'',
  socialUrlLinkedin:'',
};

const reducer = createReducer(initialState, {
  [Types.SET_VALUE]: setValueReducer,
  [Types.INITIALIZE]: () => initialState,
});

export default reducer;
