import { all, call, put, takeLeading } from 'redux-saga/effects';
import { Types, actions } from '.';

function* fetchSocialUrl() {
  yield put(actions.setValue('socialUrlGoogle', 'https://google.com'));
  yield put(actions.setValue('socialUrlFacebook', 'https://facebook.com'));
  yield put(actions.setValue('socialUrlTwitter', 'https://twitter.com'));
  yield put(actions.setValue('socialUrlLinkedin', 'https://linkedin.com'));
}
function* fetchSignIn({id, password}) {
  // yield put(actions.setValue('name', id));
  localStorage.setItem('name', id);
}
function* fetchSignUp({id, password, confirmPassword, email}) {
}

export default function* () {
  yield all([
    takeLeading(
      Types.FETCH_SOCIAL_URL,
      fetchSocialUrl,
    ),
    takeLeading(
      Types.FETCH_SIGN_IN,
      fetchSignIn,
    ),
    takeLeading(
      Types.FETCH_SIGN_UP,
      fetchSignUp,
    ),
  ]);
}
