import { createStore, combineReducers, compose, applyMiddleware, Store } from 'redux';
import createSagaMiddleware, {Task} from 'redux-saga';
import { all } from 'redux-saga/effects';
import {MakeStore, createWrapper, Context, HYDRATE} from 'next-redux-wrapper';

import headerReducer from '../Header/state';
import headerSaga from '../Header/state/saga';

import authReducer from '../Auth/state';
import authSaga from '../Auth/state/saga';

import sidebarReducer from '../SideBar/state';
import sidebarSaga from '../SideBar/state/saga';

import dashboardReducer from '../Dashboard/state';
import dashboardSaga from '../Dashboard/state/saga';

const reducer = combineReducers({
  header: headerReducer,
  auth: authReducer,
  sidebar: sidebarReducer,
  dashboard: dashboardReducer,
});

export const makeStore = (context) => {
  // 1: Create the middleware
  const sagaMiddleware = createSagaMiddleware();

  // 2: Add an extra parameter for applying middleware:
  const store = createStore(
    reducer,
    compose(applyMiddleware(sagaMiddleware))
  );

  // 3: Run your sagas on server
  store.sagaTask = sagaMiddleware.run(rootSaga);

  // 4: now return the store:
  return store;
}

function* rootSaga() {
  yield all([
    headerSaga(),
    authSaga(),
    sidebarSaga(),
    dashboardSaga(),
  ]);
}

export const wrapper = createWrapper(makeStore, {debug: true});